angular.module('starter.controllers', [])

  .controller('AppCtrl', function ($scope, $ionicModal, $timeout) {

    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function () {
      $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
      console.log('Doing login', $scope.loginData);

      // Simulate a login delay. Remove this and replace with your login
      // code if using a login system
      $timeout(function () {
        $scope.closeLogin();
      }, 1000);
    };
  })


  .controller('HostsCtrl', function ($scope) {

    $scope.getHosts = function() {
      return $resource('localhost:8090/#/rest/users', {}).query({
        method:'GET', isArray:true
      }, function(result) {
        $scope.hosts = result;
      });
    };


    /*$scope.hosts = [
      {name: 'John Doe', rating: 1, id: 1, image: "img/johnDoe.jpg", address:'Via Caraglio, 97, Torino'},
      {name: 'Jane Moe', rating: 2, id: 2, image: "img/janeDoe.jpg"},
      {name: 'Kai Toe', rating: 3, id: 3, image: "img/kaiToe.jpg"},
      {name: 'Mike Foe', rating: 4, id: 4, image:"img/mikeFoe.jpg"},
      {name: 'Jenna Woe', rating: 5, id: 5, image: "img/jennaWoe.jpg"},
      {name: 'Laura Loe', rating: 6, id: 6}
    ];*/


  })
  .controller('HostCtrl', function ($scope, $ionicPopup) {

    $scope.confirmAlert = function() {
      $ionicPopup.alert({
        title: 'Request Sent!'
      });
    };

  })



  .controller('RecommendationsCtrl', function ($scope, $ionicPopup, $ionicListDelegate) {
    $scope.recommendations = [
      {id: 1, name: 'Best Pizza', type: 'Pizza Restaurant'},
      {id: 2, name: 'Best Pasta', type: 'Pasta Restaurant'},
      {id: 3, name: 'Best Salad', type: 'Salad Restaurant'},
      {id: 4, name: 'Best Sushi', type: 'Sushi Restaurant'},
      {id: 5, name: 'No Meat', type: 'Vegan Restaurant'}
    ];

    $scope.newRecommendation = function () {
      $scope.data = {};
      var myPopup = $ionicPopup.show({
        title: "New recommendation",
        template: '<input type="text" placeholder="Type" ng-model="data.type"><br><input type="text" placeholder="Name" ng-model="data.name">',
        scope: $scope,
        buttons: [
          {text: 'Cancel'},
          {text: '<b>Save</b>', type: 'button-positive',
          onTap: function(e) {
              return $scope.data;
          }
        }]
      });

      myPopup.then(function(res) {
        if (res) {
          $scope.recommendations.push({name: res.name, type: res.type});
        }});
    };

    $scope.edit = function (recommendation) {
      $scope.data = recommendation;
      $ionicPopup.show({
        title: "Edit recommendation",
        template: '<input type="text" placeholder="Type" ng-model="data.type"><br><input type="text" placeholder="Name" ng-model="data.name">',
        scope: $scope,
        buttons: [
          {text: 'Cancel'},
          {text: '<b>Save</b>', type: 'button-positive',
            onTap: function(e) {
              return $scope.data;
            }
          }]
      }).then(function (res) {    // promise
        if (res !== undefined){
          recommendation.name = $scope.data.name;
          recommendation.type = $scope.data.type;
        }
        $ionicListDelegate.closeOptionButtons();
      })
    };
  })
  .controller('RecommendationCtrl', function ($scope, $stateParams) {
  })



  .controller('RequestsCtrl', function ($scope) {
    $scope.requests = [
      {name: 'Jane Moe', status: 2, image: "img/janeDoe.jpg"},
      {name: 'John Doe', status: 1, image: "img/johnDoe.jpg"}
    ];

  });
