// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ionic-ratings'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'templates/home.html'
          }
        }
      })

      .state('app.hosts', {
        url: '/hosts',
        views: {
          'menuContent': {
            templateUrl: 'templates/hosts.html',
            controller: 'HostsCtrl'
          }
        }
      })

      .state('app.host', {
        url: '/hosts/:hostId',
        views: {
          'menuContent': {
            templateUrl: 'templates/host.html',
            controller: 'HostCtrl'
          }
        }
      })

      .state('app.recommendations', {
        url: '/recommendations',
        views: {
          'menuContent': {
            templateUrl: 'templates/recommendations.html',
            controller: 'RecommendationsCtrl'
          }
        }
      })

      .state('app.recommendation', {
        url: '/recommendations/:recommendationId',
        views: {
          'menuContent': {
            templateUrl: 'templates/recommendation.html',
            controller: 'RecommendationCtrl'
          }
        }
      })

      .state('app.requests', {
        url: '/requests',
        views: {
          'menuContent': {
            templateUrl: 'templates/requests.html',
            controller: 'RequestsCtrl'
          }
        }
      })

      ;
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/home');
  });
