package eu.hostbroker.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Broker extends IdEntity {

    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
