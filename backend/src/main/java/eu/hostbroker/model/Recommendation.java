package eu.hostbroker.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Recommendation extends IdEntity {

    @Column(name = "name")
    private String name;

    @Column
    private String type;

}
