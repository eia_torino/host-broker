package eu.hostbroker.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 *
 */
@Configuration
public class EmailConfig {

    public static final String EMAIL_ADDRESS = "local.suggest@gmail.com";
    public static final String EMAIL_PASSWORD = "grandslam";

    @Lazy
    @Bean
    public JavaMailSenderImpl javaMailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setPort(587);
        javaMailSender.setProtocol("smtp");
        javaMailSender.setPassword(EMAIL_PASSWORD);
        javaMailSender.setUsername(EMAIL_ADDRESS);

        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.from", EMAIL_ADDRESS);
        javaMailProperties.put("mail.smtp.user", EMAIL_ADDRESS);

        javaMailProperties.put("mail.smtp.auth", true);
        javaMailProperties.put("mail.smtp.starttls.enable", true);
        javaMailProperties.put("mail.smtp.quitwait", false);
        javaMailSender.setJavaMailProperties(javaMailProperties);

        return javaMailSender;
    }


}
