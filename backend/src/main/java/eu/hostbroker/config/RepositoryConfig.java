package eu.hostbroker.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "eu.hostbroker.repository")
@EnableAutoConfiguration
@EntityScan(basePackages = {"eu.hostbroker.model"})
public class RepositoryConfig {
}
