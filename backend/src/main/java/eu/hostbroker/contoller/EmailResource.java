package eu.hostbroker.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.MimeMessage;

/**
 *
 */
@RestController
@RequestMapping(value="/rest/email")
public class EmailResource {

    @Autowired
    private JavaMailSenderImpl javaMailSender;

    @RequestMapping(method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendGeneric() throws Exception {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mailMsg = new MimeMessageHelper(mimeMessage);
        mailMsg.setFrom("local.suggest@gmail.com");
        mailMsg.setTo("vikreinok@gmail.com");
        mailMsg.setSubject("Test mail");
        mailMsg.setText("Hello World!");
        javaMailSender.send(mimeMessage);
    }


}
