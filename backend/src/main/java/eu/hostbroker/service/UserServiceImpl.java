package eu.hostbroker.service;

import eu.hostbroker.dto.UserDTO;
import eu.hostbroker.model.AppUser;
import eu.hostbroker.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    @Transactional(readOnly = true)
    public List<UserDTO> getAll() {
        List<AppUser> appUsers = userRepository.findAll();

        //TODO
        List<UserDTO> userDTOs = new ArrayList<>();
        for (AppUser appUser : appUsers) {
            UserDTO userDTO = new UserDTO();
            userDTO.setFirstName(appUser.getFirstName());
            userDTO.setLastName(appUser.getLastName());
            userDTO.setEmail(appUser.getEmail());
            userDTO.setAbLink(appUser.getAbLink());
            userDTO.setPhone(appUser.getPhone());
            userDTO.setRating(appUser.getRating());
            userDTOs.add(userDTO);
        }
        return userDTOs;
    }
}
