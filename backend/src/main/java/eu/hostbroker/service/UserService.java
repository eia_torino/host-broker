package eu.hostbroker.service;

import eu.hostbroker.dto.UserDTO;

import java.util.List;

public interface UserService {

    List<UserDTO> getAll();

}
