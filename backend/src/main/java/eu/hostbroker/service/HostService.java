package eu.hostbroker.service;

import eu.hostbroker.dto.HostDTO;

import java.util.List;

public interface HostService {

    List<HostDTO> getHosts();

}
